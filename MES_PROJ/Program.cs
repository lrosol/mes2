﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using MES.Model;
using System.Reflection;
using System.IO;
using Newtonsoft.Json;

namespace MES_PROJ
{
    static class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            if(args != null)
            {
                Console.WriteLine("Hello MES!\n");
                for (int i = 0; i < args.Length; ++i)
                {
                    // Validating files with arguments and execute computing for each in separate thread
                    if (args[i].Substring(args[i].LastIndexOf(".")+1) == "json" && File.Exists(args[i]))
                    {
                        ThreadPool.QueueUserWorkItem(new WaitCallback(Computing), args[i]);
                    }
                }
                Console.ReadLine();
            }
        }

        private static readonly object ConsoleWriterLock = new object();
        private static void Computing(Object state)
        {
            string filename = state as string;
            Console.WriteLine("ThreadID: " + Thread.CurrentThread.ManagedThreadId +
                "\n" + "Reading: " + filename.Substring(filename.LastIndexOf("\\")+1));
            string output = "";
            int color = (int)ConsoleColor.White;
            try
            {
                // Read data from file
                InputData iData = InputData.ReadJson(filename);

                // Initialize matrixes
                GlobalGrid global = new GlobalGrid(iData.dT, iData.R, iData.Elements, iData.T);

                // Preapring container for computing results
                Dictionary<double, List<double>> Temperatures = new Dictionary<double, List<double>>();

                // Execute computing in time loop
                for (double time = iData.dT; time <= iData.Tau; time += iData.dT)
                {
                    double[] result = LinearEquationSolver.Solve(global.GetK(), global.GetF());

                    Temperatures.Add(time, new List<double>());
                    for (int r = 0; r < result.Length; ++r)
                    {
                        Temperatures[time].Add(Math.Round(result[r], iData.Precision));
                    }
                    global.setTemperatures(Temperatures[time]);
                }

                output = JsonConvert.SerializeObject(Temperatures);
                color = (int)ConsoleColor.DarkGreen;
            }
            catch (Exception ex)
            {
                output = JsonConvert.SerializeObject(ex);
                color = (int)ConsoleColor.Red;
            }
            finally
            {
                lock (ConsoleWriterLock)
                {
                    Console.ForegroundColor = (ConsoleColor)color;
                    using (StreamWriter writer = new StreamWriter(filename.Insert(filename.Length - 5, "_out")))
                    {
                        Console.WriteLine("ThreadID: " + Thread.CurrentThread.ManagedThreadId +
                            "\n" + "Writing: " + filename.Insert(filename.Length - 5, "_out").Substring(filename.LastIndexOf("\\") + 1));
                        writer.Write(output);
                    }
                    Console.ResetColor();
                }
            }
        }
    }
}
