﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MES.Model;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;

namespace UnitTests
{
    [TestClass]
    public class JsonIO
    {
        private const string DefaultJsonSource = "JsonIO.json";
        private const string DefaultJsonOutput = "JsonIO.json";

        [TestMethod]
        public void WriteInputDataClass()
        {
            Dictionary<string, double> bc1 = new Dictionary<string, double>(){{"t_oo", 34.43},{"a", 23}};
            Dictionary<string, double> bc2 = new Dictionary<string, double>(){{"t", 54},{"re", 43}};
            Element el1 = new Element(1, 2, 3, 4, 5, new int[]{1,2}, new List<Dictionary<string, double>>(){bc1, bc2});

            Element el2 = new Element(5, 4, 3, 2, 1, new int[]{2,3}, new List<Dictionary<string, double>>() { });
            List<Element> elements = new List<Element> { el1, el2 };
            List<double> temp = new List<double> { 1, 1, 1, 1, 1, 1, 1 };
            InputData iData = new InputData(7, 9, temp, 10, elements);

            using (StreamWriter w = new StreamWriter(DefaultJsonOutput))
            {
                string str = JsonConvert.SerializeObject(iData);
                w.Write(str);
                Console.WriteLine(str);
            }
        }

        [TestMethod]
        public void ReadInputDataClass()
        {
            InputData iData = InputData.ReadJson(DefaultJsonSource);

            Assert.AreEqual(7, iData.Tau);
            Assert.AreEqual(1, iData.T[0]);
            Assert.AreEqual(34.43, iData.Elements[0].BC[0]["t_oo"]);
            Assert.AreEqual(43, iData.Elements[0].BC[1]["re"]);
            Assert.AreEqual(3, iData.Elements[1].C);
        }
    }

    [TestClass]
    public class RunTest
    {
        [TestMethod]
        public void Run1()
        {
            InputData iData = InputData.ReadJson("input.json");
            List<Element> elements = iData.Elements;
            Dictionary<double, List<double>> Temperatures = new Dictionary<double, List<double>>();
            GlobalGrid global = new GlobalGrid(iData.dT, iData.R, elements, iData.T);
            for(double time = iData.dT; time <= iData.Tau; time+=iData.dT)
            {
                var A = Matrix<double>.Build.DenseOfArray(global.GetK());
                var b = Vector<double>.Build.Dense(global.GetF());
                double[] result = A.Solve(b).ToArray();

                Temperatures.Add(time, new List<double>());
                for(int r=0; r<result.Length; ++r)
                {
                    Temperatures[time].Add(Math.Round(result[r], iData.Precision));
                }
                global.setTemperatures(Temperatures[time]);
            }

            using (StreamWriter w = new StreamWriter("result.json"))
            {
                string str = JsonConvert.SerializeObject(Temperatures);
                w.Write(str);
                Console.WriteLine(str);
            }


            //double[,] ma = global.GenerateK();
            //Assert.AreEqual(81, ma.Length);
            //for (int i = 0; i < 8; ++i)
            //{
            //    for (int j = 0; j < 8; ++j)
            //    {
            //        //Console.Write("[" + i + "," + j + "]: ");
            //        Console.Write(ma[i, j] + ", ");
            //    }
            //    Console.Write("\n");
            //}

            //double[] fa = global.GenerateF();
            //for (int i = 0; i < fa.Length; ++i)
            //{
                
            //    Console.WriteLine(fa[i]);

            //}

            //var A = Matrix<double>.Build.DenseOfArray(global.GenerateK());
            //var b = Vector<double>.Build.Dense(global.GenerateF());
            //double[] result = A.Solve(b).ToArray();
            //Console.Write("Results");
            //for (int i = 0; i < result.Length; ++i)
            //{
            //    Console.WriteLine(result[i]);
            //}

            //double[,] matrix = global.LinearEquations();
            //LinearEquationSolver.Solve(matrix);
            //double[] result = LinearEquationSolver.OnlyResults(ref matrix);
            //Console.Write("Results");
            //for(int i=0; i<result.Length; ++i)
            //{
            //    Console.WriteLine(result[i]);
            //}

            //Console.Write(ma[0, 2] + ", ");
            //Console.Write(ma[2, 2] + ", ");
        }
    }
}
