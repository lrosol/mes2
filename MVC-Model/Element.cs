﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;

namespace MES.Model
{
    public class Element
    {
        [JsonConstructor]
        public Element(double r1, double r2, double c, double p, double k, int[] nop, List<Dictionary<string, double>> bc)
        {
            R1 = r1;
            R2 = r2;
            C = c;
            Ro = p;
            K = k;
            NOP = nop;
            BC = bc;
        }

        public static List<Element> ReadJson(string source = Element.DefaultJsonSource)
        {
            using (StreamReader r = new StreamReader(source))
            {
                string str = r.ReadToEnd();
                return JsonConvert.DeserializeObject<List<Element>>(str);
            }
        }
        public static void WriteJson(List<Element> bs, string output = Element.DefaultJsonOutput)
        {
            using (StreamWriter w = new StreamWriter(output))
            {
                string str = JsonConvert.SerializeObject(bs);
                w.Write(str);
            }
        }

        public double R1
        {
            get { return _R1; }
            set { _R1 = value; }
        }
        public double R2
        {
            get { return _R2; }
            set { _R2 = value; }
        }
        public double C
        {
            get { return _C; }
            set { _C = value; }
        }
        public double Ro
        {
            get { return _Ro; }
            set { _Ro = value; }
        }
        public double K
        {
            get { return _K; }
            set { _K = value; }
        }
        public int[] NOP
        {
            get { return _NOP; }
            set { _NOP = value; }
        }
        public List<Dictionary<string, double>> BC
        {
            get { return _BC; }
            set { _BC = value; }
        }

        private const string DefaultJsonSource = "input.json";
        private const string DefaultJsonOutput = "output.json";

        private double _R1;
        private double _R2;
        private double _C;      
        private double _Ro;
        private double _K;
        private int[] _NOP;
        private List<Dictionary<string, double>> _BC;
    }  
}
