﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Model
{
    public class GlobalGrid
    {
        private double _DT;
        private double _R_MAX;
        private List<Element> Elements;
        private List<double> T;
        private List<LocalGrid> Locals;

        public GlobalGrid(double dt, double r, List<Element> el, List<double> t)
        {
            if (el.Count+1 != t.Count)
                throw new Exception(this.GetType().Name + ": Amount of nodes and temperatures are different " + el.Count + ", " + (t.Count+1));
            _DT = dt;
            _R_MAX = r;
            Elements = el;
            T = t;
            Locals = new List<LocalGrid>();
            Aggregate();
        }

        public GlobalGrid(double dt, double t0, double r, List<Element> el)
        {
            _DT = dt;
            _R_MAX = r;
            Elements = el;
            T = new List<double>();
            for (int i = 0; i <= el.Count; ++i)
            {
                T.Add(t0);
            }
            Locals = new List<LocalGrid>();
            Aggregate();
        }

        public double[,] GetK()
        {
            int dim = NodesAmount();

            double[,] G = new double[dim, dim];
            foreach (LocalGrid s in Locals)
            {
                for (int i = 0; i < s.NOP.Length; ++i)
                {
                    for (int j = 0; j < s.NOP.Length; ++j)
                    {
                        Point<int, double> p = s.GetK[i, j];
                        int x = p.getCoordiante((int)Dim.x);
                        int y = p.getCoordiante((int)Dim.y);
                        try
                        { 
                            G[x,y] += p.Value;
                        }
                        catch (Exception ex)
                        {
                            System.Console.WriteLine();
                            if(x >= G.GetLength(0) || y >= G.GetLength(1))
                            {
                                throw new Exception("Can't generate global matrix. Please make sure that number of points is equal to the higher index points. Index out of range for G[ " + x + ", " + y + " ]");
                            }
                            throw ex;
                        }
                    }
                }
            }
            return G;
        }

        public double[] GetF()
        {
            int dim = NodesAmount();

            double[] p = new double[dim];
            foreach(LocalGrid s in Locals)
            {
                for(int i=0; i<s.F.Length; ++i)
                {
                    try
                    {
                        p[s.NOP[i]] += -1 * s.F[i];
                    }
                    catch(Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                }
            }

            return p;
        }

        public List<LocalGrid> Aggregate()
        {
            Locals.Clear();
            for(int i=0; i<Elements.Count; ++i)
            {
                Locals.Add(new LocalGrid(_DT, T[i], T[i + 1], _R_MAX, Elements[i]));
            }
            return Locals;
        }

        private int NodesAmount()
        {
            List<int> uniqueNodes = new List<int>();

            foreach(LocalGrid s in Locals)
            {
                for (int i = 0; i < s.NOP.Length; i++)
                {
                    int val = s.NOP[i];
                    if (!uniqueNodes.Exists(x => x == val))
                    {
                        uniqueNodes.Add(val);
                    }
                }
            }

            return uniqueNodes.Count;
        }

        public void setTemperatures(List<double> t)
        {
            T = t;
            Aggregate();
        }
    }
}
