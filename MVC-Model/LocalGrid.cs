﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using Newtonsoft.Json;

namespace MES.Model
{
    public class LocalGrid : Element
    {
        public LocalGrid(double dt, double r1, double r2, double rmax, double c, double p, double k, int[] nop, double t1, double t2, List<Dictionary<string, double>> bc) :
            base(r1, r2, c, p, k, nop, bc)
        {
            dR = R2 - R1;
            dT = dt;
            R_MAX = rmax;
            // reserve memory
            _NM = new int[2, 2] { { 1, -1 }, { -1, 1 } };
            F = new double[] { 0, 0 };
            T0 = new double[2];
            T0[0] = t1;
            T0[1] = t2;
            initializeLocalMatrix(out _K, 2);

            // fill matrix _K values from NOP
            Aggreagate();
        }

        public LocalGrid(double dt, double t1, double t2, double rmax, Element el)
            : this(dt, el.R1, el.R2, rmax, el.C, el.Ro, el.K, el.NOP, t1, t2, el.BC) {}

        public Point<int, double>[,] GetK
        {
            get {return _K; }
        }
        private void initializeLocalMatrix(out Point<int, double>[,] matrix, int dim)
        {
            // initialize empty matrix _K
            matrix = new Point<int, double>[NOP.Length, NOP.Length];
            for (int i = 0; i < NOP.Length; ++i)
            {
                for (int j = 0; j < NOP.Length; ++j)
                {
                    matrix[i, j] = new Point<int, double>(dim);
                }
            }
        }
        private void Aggreagate()
        {
            // Preparing equations
            double[] r = new double[2];
            r[0] = Transformation.Calc(Transformation.ksi[0], R1, R2);
            r[1] = Transformation.Calc(Transformation.ksi[1], R1, R2);

            double[] ksi = new double[2];
            ksi[0] = Transformation.ksi[0];
            ksi[1] = Transformation.ksi[1];

            Transformation.NFunction[] N = new Transformation.NFunction[2];
            N[0] = Transformation.N1;
            N[1] = Transformation.N2;

            // Computing
            double part1 = K * (r[0] + r[1]) / dR;
            double part2 = C * Ro * dR / dT;
            double part3 = 0;

            for (int i = 0; i < NOP.Length; ++i)
            {
                for (int j = 0; j < NOP.Length; ++j)
                {
                    _K[i, j].setCoordinate((int)Dim.x, NOP[i]);
                    _K[i, j].setCoordinate((int)Dim.y, NOP[j]);

                    part3 = 0;
                    for (int k=0; k < 2; ++k)
                    {
                        part3 += r[k] * N[i](ksi[k]) * N[j](ksi[k]);
                    }

                    // Most important part
                    _K[i, j].Value = _NM[i % NOP.Length, j % NOP.Length] * part1 + part2 * part3;
                }

                part3 = 0;
                for (int k = 0; k < 2; ++k )
                {
                    part3 += Transformation.Calc(Transformation.ksi[k], T0[0], T0[1])*r[k]*N[i](ksi[k]);
                }
                
                _F[i] = -1 * part2 * part3;

                if (BC[i].ContainsKey("a"))
                {
                    //Console.WriteLine("a " + 2* R_MAX * BC[i]["a"]);
                    _K[i, i].Value += 2 * BC[i]["a"] * R_MAX;
                    if (BC[i].ContainsKey("t"))
                    {
                        F[i] += -2 * BC[i]["a"] * R_MAX * BC[i]["t"];
                    }
                }
            }
        }

        public double[] F
        {
            get { return _F; }
            set { _F = value; }
        }

        private double[] _F;
        private Point<int, double>[,] _K;
        private int[,] _NM;
        private double[] T0;
        private double dR;
        private double dT;
        private double R_MAX;

    }
}
