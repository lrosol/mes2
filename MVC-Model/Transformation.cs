﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Model
{
    public static class Transformation
    {
        public static readonly double[] ksi = {0.5773, -0.5773};

        // Shape functions

        public delegate double NFunction(double x);

        public static double N1(double ksi)
        {
            return (1-ksi)/2;
        }
        public static double N2(double ksi)
        {
            return (1+ksi)/2;
        }

        // Transform local to global coordinate system
        public static double Calc(double ksi, double p0, double p1)
        {	
            return N1(ksi)*p0 + N2(ksi)*p1;
        }
    }
}
