﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MES.Model
{
    public class Point<T, V>
    {
        public Point(int dimension)
        {
            Coordinates = new T[dimension];
        }
        [JsonConstructor]
        public Point(T[] coordinates, V value):this(coordinates.Length)
        {
            for (int i = 0; i < coordinates.Length; ++i)
            {
                Coordinates[i] = coordinates[i];
            }

            Value = value;
        }

        public int getDimension()
        {
            return Coordinates.Length;
        }

        public bool setCoordinate(int i, T value)
        {
            try
            {
                Coordinates[i] = value;
                return true;
            }
            catch (System.IndexOutOfRangeException)
            {
                return false;
            }
        }

        public T getCoordiante(int i)
        {
            try
            {
                return Coordinates[i];
            }
            catch (System.IndexOutOfRangeException ex)
            {
                System.ArgumentException argEx = new System.ArgumentException("Index is out of range", "i", ex);
                throw argEx;
            }
        }

        public V Value
        {
            get { return _value; }
            set { _value = value; }
        }

        public T[] Coordinates;
        private V _value;
    }
}
