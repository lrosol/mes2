﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MES.Model
{
    public class InputData
    {
        [JsonConstructor]
        public InputData(double tau, double dt, List<double> t, double r_max, List<Element> el, int prec = 2)
        {
            Tau = tau;
            dT = dt;
            T = t;
            R = r_max;
            Elements = el;
            Precision = prec;
        }

        public InputData() : this(0, 0, new List<double>(), 0, new List<Element>(), 2) { }

        public static InputData ReadJson(string file = InputData.DefaultJsonSource)
        {
            string strData;
            using(StreamReader reader = new StreamReader(file))
            {
                strData = reader.ReadToEnd();
            }
            return JsonConvert.DeserializeObject<InputData>(strData);
        }

        public double Tau
        {
            get { return _TAU; }
            set { _TAU = value; }
        }

        public double dT
        {
            get { return _DT; }
            set { _DT = value; }
        }

        public List<double> T
        {
            get { return _T; }
            set { _T = value; }
        }

        public double R
        { 
            get { return _R_MAX; }
            set { _R_MAX = value; }
        }

        public List<Element> Elements
        {
            get { return _ELEMENTS; }
            set { _ELEMENTS = value; }
        }

        public int Precision
        { 
            get { return _PRECISION; }
            set { _PRECISION = value; }
        }

        private double _TAU;
        private double _DT;
        private List<double> _T;
        private double _R_MAX;
        private int _PRECISION;
        private List<Element> _ELEMENTS;

        private const string DefaultJsonSource = "input.json";
        private const string DefaultJsonOutput = "output.json";
    }
}
